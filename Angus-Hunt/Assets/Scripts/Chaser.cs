using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Chaser : MonoBehaviour {
	
	public float speed = 0.2f;
	public float minDist = 1f;
	public GameObject destination;
	private NavMeshAgent agent;

	// Use this for initialization
	void Start () 
	{
		destination = GameObject.FindGameObjectWithTag("Destination");
		agent = GetComponent<NavMeshAgent>();
		//player = FindAnyObjectByType<PlayerMovement>().transform;
		agent.SetDestination(destination.transform.position);

		/* if no target specified, assume the player
		if (player == null) {

			if (GameObject.FindWithTag ("Player")!=null)
			{
				player = GameObject.FindWithTag ("Player").GetComponent<Transform>();
			}
		}
		*/
	}
	
	/* Update is called once per frame
	void Update () 
	{
		if (player == null)
			return;

		// face the target
		transform.LookAt(player);

		//get the distance between the chaser and the target
		//float distance = Vector3.Distance(transform.position,target.position);

		//so long as the chaser is farther away than the minimum distance, move towards it at rate speed.
		//if(distance > minDist)

		//transform.position += transform.forward * speed * Time.deltaTime;
		agent.SetDestination(player.position);
	}

	// Set the target of the chaser
	public void SetTarget(Transform newTarget)
	{
		player = newTarget;
	}
	*/
}
