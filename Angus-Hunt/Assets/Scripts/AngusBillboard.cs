using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AngusBillboard : MonoBehaviour
{
    public float rotationSpeed = 0;
    public GameObject player;

    // Update is called once per frame
    void Update()
    {
            transform.Rotate(new Vector3(0, rotationSpeed * Time.deltaTime, 0));
            //transform.LookAt(player.transform);
            //transform.position += transform.forward * 0.2f * Time.deltaTime;
    }
}
